import {
  View,
  Text,
  Image,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Button, TextInput} from 'react-native-paper';
import axios from 'axios';
import qs from 'qs';
import API from '../Utils/Api';
import BASEURL from '../Utils/Config';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const URL = BASEURL + 'loginProcess';

  const loginProcess = async () => {
    // return  getData()
    var formdata = new FormData();
    formdata.append('email', email);
    formdata.append('password', password);
    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow',
    };
    await fetch(URL, requestOptions)
      .then(response => response.text())
      .then(res => {
        console.log(res);
        let json = JSON.parse(res);
        if (json.status == 0) {
          alert(json.message);
        } else {
          const val = json.data;
          storeData(val);
          navigation.navigate('Home');

          alert(json.message);
        }
      })

      .catch(error => alert(error));
  };

  const storeData = async val => {
    try {
      const jsonValue = JSON.stringify(val);
      await AsyncStorage.setItem('login', jsonValue);
    } catch (e) {
      console.log(e);
      alert(e);
      // saving error
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('login');
      jsonValue != null ? JSON.parse(jsonValue) : null;
      console.log(jsonValue);
      if (jsonValue != null) {
        navigation.navigate('Home');
      }
    } catch (e) {
      console.log('error = >' + e);
      alert(e);
    }
  };

  const Logout = async () => {
    try {
      await AsyncStorage.removeItem('login');
    } catch (e) {
      // remove error
      alert(e);
    }

    console.log('Done.');
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8CC0DE',
      }}>
      <View>
        <Image
          style={{
            width: 200 / 1.5,
            height: 200 / 1.5,
            // flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            // height: '100%',
          }}
          source={{
            uri: 'https://cdn-icons-png.flaticon.com/512/272/272354.png',
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="Email"
          style={styles.input}
          onChangeText={text => setEmail(text)}
          // value={text}
          left={<TextInput.Icon name="form-textbox-password" />}
        />
        <TextInput
          placeholder="Password"
          style={styles.input}
          onChangeText={text => setPassword(text)}
          secureTextEntry
          left={<TextInput.Icon name="form-textbox-password" />}
        />
        <Button mode="contained" onPress={() => loginProcess()}>
          Login
        </Button>
      </View>
      <View>
        <Text>Forgot password?</Text>
      </View>
      <View>
        <Text> New User? Create Acount</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  input: {
    margin: 20,
    height: 25,
    width: 311,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    borderColor: 'blue',
    backgroundColor: 'white',
  },
});
