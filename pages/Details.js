import {View, Text, BackHandler, Alert, ScrollView} from 'react-native';
import React, {useState, useEffect} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';

import {
  // HeaderSearchBar,
  HeaderClassicSearchBar,
} from 'react-native-header-search-bar';
import DropDown from 'react-native-paper-dropdown';
import {
  Appbar,
  DarkTheme,
  DefaultTheme,
  Provider,
  Surface,
  ThemeProvider,
} from 'react-native-paper';
export default function Details({navigation, route}) {
  var slug = route.params.slug;
  if(slug==''){
    navigation.navigate('Home');
  }
  const [barang, setBarang] = useState('');
  const [colors, setColors] = useState('');
  const [dataBarang, setDataBarang] = useState([]);
  const [showMultiSelectDropDown, setShowMultiSelectDropDown] = useState(false);

  const barangHilang = [
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll hasan',
      id_barangHilang: '1',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya ',
      slug: '1-icikiwir',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll Yang Ke Dua hasan',
      id_barangHilang: '2',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya Yang Ke 2',
      slug: '2-icikiwiraq',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll',
      id_barangHilang: '1',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya ',
      slug: '1-icikiwir',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll Yang Ke Dua',
      id_barangHilang: '2',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya Yang Ke3',
      slug: '2-icikiwiraq',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll',
      id_barangHilang: '1',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya 4',
      slug: '1-icikiwir',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll Yang Ke Dua',
      id_barangHilang: '2',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya Yang Ke 5',
      slug: '2-icikiwiraq',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll',
      id_barangHilang: '1',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya ',
      slug: '1-icikiwir',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll Yang Ke Dua',
      id_barangHilang: '2',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya Yang Ke 2',
      slug: '2-icikiwiraq',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll',
      id_barangHilang: '1',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya ',
      slug: '1-icikiwir',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll Yang Ke Dua',
      id_barangHilang: '2',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya Yang Ke3',
      slug: '2-icikiwiraq',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll',
      id_barangHilang: '1',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya 4',
      slug: '1-icikiwir',
    },
    {
      cover: 'gambar.png',
      deskripsi: 'saya punya barang hilang dll Yang Ke Dua',
      id_barangHilang: '2',
      id_lokasi: '1',
      judul: 'Tolong Temukan Barang Saya Yang Ke 5',
      slug: '2-icikiwiraq',
    },
  ];
  const colorList = [
    {
      label: 'pilih',
      value: 'default',
    },
    {
      label: 'White',
      value: '1',
    },
    {
      label: 'Red',
      value: '2',
    },
    {
      label: 'Blue',
      value: '3',
    },
    {
      label: 'Green',
      value: '4',
    },
    {
      label: 'Orange',
      value: '5',
    },
  ];

  const gotoDetail = id => {
    alert(id);
  };

  useEffect(() => {
    if (barang != '') {
      let result = barangHilang.filter(o => o.deskripsi.includes(barang));
      setDataBarang(result);
    } else {
      setDataBarang(barangHilang);
    }
  }, [barang]);

  return (
    <Provider>
      <View>
        <HeaderClassicSearchBar
          onChangeText={text => setBarang(text)}
          searchBoxText="Cari Barang Hilang.."
          searchBoxOnPress={() => alert(barang)}
        />
      </View>
      <DropDown
        label={'Colors'}
        mode={'outlined'}
        visible={showMultiSelectDropDown}
        showDropDown={() => setShowMultiSelectDropDown(true)}
        onDismiss={() => setShowMultiSelectDropDown(false)}
        value={colors}
        setValue={setColors}
        list={colorList}
      />

      <ScrollView>
        {dataBarang.map((data, key) => {
          return (
            <Card id={key} onPress={() => gotoDetail(data.slug)}>
              <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
              <Card.Content
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Title>{data.judul}</Title>
                <Paragraph>{data.judul}</Paragraph>
              </Card.Content>
            </Card>
          );
        })}
      </ScrollView>
    </Provider>
  );
}
