import { View, Text, Image, ActivityIndicator, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import AppIntroSlider from 'react-native-app-intro-slider';

const slides = [
  {
    key: 1,
    title: 'Mari Kita Bantu Barang Yang Hilang',
    text: 'Description.\nSay something cool',
    image: 'https://cdn4.buysellads.net/uu/1/50174/1564282856-carbon.png',
    backgroundColor: '#59b2ab',
  },
  {
    key: 2,
    title: 'Cari Barang Hilangmu Disini',
    text: 'Other cool stuff',
    image: 'https://cdn4.buysellads.net/uu/1/50174/1564282856-carbon.png',
    backgroundColor: '#febe29',
  },
  {
    key: 3,
    title: 'Posting Barang Hilang Mu Disini',
    text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
    image: 'https://cdn4.buysellads.net/uu/1/50174/1564282856-carbon.png',
    backgroundColor: '#22bcb5',
  },
];

export default function InfoTwo({navigation}) {
  const [finish, setFinish] = useState(false)
    const _renderItem = ({item}) => {
      console.log(item)
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: item.backgroundColor,
          }}>
          <Text
            style={{
              color: 'white',
              fontSize: 27,
              fontWeight: 'bold',
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            {item.title}
          </Text>
          <Image
            style={{
              width: 200 / 1.5,
              height: 200 / 1.5,
              // flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              // height: '100%',
            }}
            source={{
              uri: item.image,
            }}
          />
          <Text
            style={{
              color: 'white',
              fontSize: 27,
              fontWeight: 'bold',
              justifyContent: 'center',
              textAlign: 'center',
            }}>
            {item.text}
          </Text>
        </View>
      );
    };
    const   _onDone = () => {
      navigation.navigate('InfoOne');      
    };
  return (
      <AppIntroSlider
        renderItem={_renderItem}
        data={slides}
        onDone={_onDone}
      />
  );
}
const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  //[...]
});