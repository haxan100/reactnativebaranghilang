import {View, Text, Image, ActivityIndicator} from 'react-native';
import React from 'react';

export default function Splash({navigation}) {
  setTimeout(() => {
    navigation.replace('Home');
  }, 2000);
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8CC0DE',
      }}>
      <Image
        source={{
          uri: 'https://cdn4.buysellads.net/uu/1/50174/1564282856-carbon.png',
        }}
        style={{
          width: 200/1.5,
          height: 200/1.5,
          // flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          // height: '100%',
        }}
      />
      <ActivityIndicator size="large" color="#00ff00" />
    </View>
  );
}
