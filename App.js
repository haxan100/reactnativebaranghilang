import {View, Text, Button} from 'react-native';
import React, {useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

const Tab = createMaterialBottomTabNavigator();
// import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {
  // HeaderSearchBar,
  HeaderClassicSearchBar,
} from 'react-native-header-search-bar';

import Home from './pages/Home';
import Splash from './pages/Splash';
import Login from './pages/Login';
import InfoTwo from './pages/InfoTwo';
import Profile from './pages/Profile';
import Details from './pages/Details';

export default function App() {
  const Stack = createNativeStackNavigator();

  function HomeScreen({navigation}) {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />
      </View>
    );
  }
  function DetailsScreen({navigation}) {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button
          title="Go to Details... again"
          onPress={() => navigation.navigate('Home')}
        />
      </View>
    );
  }

  const [barang, setBarang] = useState('App');
  const [showCari, setShowCari] = useState(true);

  if (barang == 'icikiwir') alert('halah');

  function MyTabs() {
    return (
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          showIcon: true,
        }}>
        <Tab.Screen
          options={{tabBarBadge: 3}}
          name="Home"
          component={Home}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color, size}) => (
              <Ionicons name="home" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{
            tabBarLabel: 'HomeScreen',
            tabBarIcon: ({color, size}) => (
              <Ionicons name="planet-outline" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Detailscr"
          component={DetailsScreen}
          options={{
            tabBarLabel: 'Details',
            tabBarIcon: ({color, size}) => (
              <Ionicons name="information-outline" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }

  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          {/* <Stack.Screen name="InfoTwo" component={InfoTwo} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Profile" component={Profile} /> */}
          <Stack.Screen name="Home" component={MyTabs} />
          <Stack.Screen name="Details" component={Details} />
          {/* <Stack.Screen name="Splash" component={Splash} />
          <Stack.Screen name="HomeScreen" component={HomeScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} /> */}
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}
